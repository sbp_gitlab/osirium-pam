# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name   OsiriumPAM.ps1          
# Description   PRTG EXE/Script Sensor to query Osirium PAM cloud API.  
#
#-------------------
# Requirements
#
# Osirium PAM Cloud Instance
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      27/02/2023  Initial Release
#
# ------------------
# (c) 2023 Simon Bell | Paessler AG
# ------------------

# Define parameters to be passed from PRTG

Param (
		[string]$clientID = $null,
		[string]$token = $null,
		[string]$source = $null,
		[string]$serverIP = $null,
	[int]$debuglevel=0
)


############ Bypass Cert errors in Powershell 5.x ############

if (-not("dummy" -as [type])) {
    add-type -TypeDefinition @"
using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public static class Dummy {
    public static bool ReturnTrue(object sender,
        X509Certificate certificate,
        X509Chain chain,
        SslPolicyErrors sslPolicyErrors) { return true; }

    public static RemoteCertificateValidationCallback GetDelegate() {
        return new RemoteCertificateValidationCallback(Dummy.ReturnTrue);
    }
}
"@
}

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = [dummy]::GetDelegate()

############ Bypass Cert errors in Powershell 5.x ############

#Validate passed parameters


if (-not $clientID) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide clientID </text>
</prtg>
"@
}

if (-not $token) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide API token</text>
</prtg>
"@
}

if (-not $source) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide report source keyword </text>
</prtg>
"@
}

if (-not $serverIP) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide the Osirium server IP </text>
</prtg>
"@
}

#Construct API call

$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("Content-Type", "application/json")
$headers.Add("X-PXM-API-CLIENT-ID", $clientID)
$headers.Add("X-PXM-API-TOKEN", $token)

#Send API call

$url = ("http://" + $serverIP + "/pxm_api/v1/" + $source)

$response = Invoke-RestMethod -uri $url -Method 'GET' -Headers $headers
$response | ConvertTo-Json | out-null
$count = $response.length

#Map API return to Sensor channel

Write-Host 
"<prtg>"
    "<result>"
        "<channel>'User Count'</channel>"
        "<value>"+ [int]$count +"</value>"
    "</result>"
"</prtg>"