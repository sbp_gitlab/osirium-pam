# Osirium-PAM Sensor

PRTG sensors to retrieve asset information from Osirium Privileged Access Management server

## Introduction

How do you control who is accessing which resources in your environment, whether from inside or outside the network?

Osirium’s Privileged Access Management Server makes it easy for Sysadmins to administer, control and audit who is accessing which assets, and what they’re doing while connected.

The system is API enabled, which means PRTG can easily collect status and performance data from the Osirium platform. This project includes a PowerShell based EXE/Script Advanced sensor to do just that. 


## Installation and Usage

Copy the included OsiriumPAM.ps1 file to the EXEXML custom sensor folder on your PRTG server (default - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\EXEXML).

You'll need to add API crdentials from the Osirium portal and pass some configuration parameters to the scrip. Full details of how to do both can be found in this accompanying blog article:

https://blog.paessler.com/privileged-access-management-the-network-gatekeeper


